/*
Author : Samuel Beland-Leblanc (7185642)
File : CollisionZone.h

The colision zone manages the colision detection for a sub part of the screen.

*/

#pragma once
/*
Author : Samuel Beland-Leblanc (7185642)
File : CollisionZone.h

The colision zone manages the colision detection for a sub part of the screen.

*/

#include <vector>
#include "Circle.h"

using namespace std;

class CollisionZone
{
private:
	//bounds of the zone
	float _upper_bound;
	float _lower_bound;
	float _right_bound;
	float _left_bound;

	vector<Circle*>* _circles_to_check; //list of circles in the zone OR overlapping it


public:

	CollisionZone(float leftB, float upperB, float rightB, float lowerB)  ;
	~CollisionZone();

	void AddCircleToCheck(Circle *);
	void CheckForCollisions();

	int getNumOfCirclesToCheck(){ return _circles_to_check->size(); };

	float getUpperBound() { return _upper_bound; };
	float getRightBound() { return _right_bound; };
	float getLowerBound() { return _lower_bound; };
	float getLeftBound() { return _left_bound; };

	bool CheckForDuplicate(CollisionZone* const);

};

