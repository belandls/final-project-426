
#include "kernel.h.cl"

__kernel void UpdateKernel(__global Position* ballPosition, __global Velocity* ballVelocity, float acceleration, float radius, __global Position* posGpuBuf, int n){
	ballVelocity->yVel -= acceleration; //Ajust the velocity with the gravity
	//Update the position of the circle
	ballPosition->x += ballVelocity->xVel;
	ballPosition->y += ballVelocity->yVel;

	//Basic boundaries collision detections
	if (ballPosition->y + radius > 1.0f || ballPosition->y - radius < -1.0f){
		if (ballPosition->y + radius > 1.0f){
			//Make a "sticky" top so that the balls don't go too crazy
			ballVelocity->yVel = 0 - acceleration *1.2;
		}
		else{
			ballVelocity->yVel *= -1;
		}
		//If the ball goes out of bounds, bring it back in
		while (ballPosition->y + radius > 1.0f || ballPosition->y - radius < -1.0f){
			ballPosition->y += ballVelocity->yVel;
		}
	}
	if (ballPosition->x + radius > 1.0f || ballPosition->x - radius < -1.0f){
		ballVelocity->xVel *= -1;
		//If the ball goes out of bounds, bring it back in
		while (ballPosition->x + radius > 1.0f || ballPosition->x - radius < -1.0f){
			ballPosition->x += ballVelocity->xVel;
		}
	}
	posGpuBuf[n].x = ballPosition->x;
	posGpuBuf[n].y = ballPosition->y;

}

float GetDistance(__global Position* p1, __global  Position* p2){
	float x = p1->x - p2->x;
	float y = p1->y - p2->y;
	return sqrt((x*x) + (y*y));
}

//Atomic move when unsticking balls
void SimplifiedMove(__global Position* p, __global Velocity* v, float r){

	atomic_xchg(&(p->x), (p->x)+ v->xVel);
	atomic_xchg(&(p->y), (p->y)+ v->yVel);
	/*p->x += v->xVel;
	p->y += v->yVel;*/

	if (p->y + r > 1.0f || p->y - r < -1.0f){
		v->yVel *= -1;
		while (p->y + r > 1.0f || p->y - r < -1.0f){
			//p->y += v->yVel;
			atomic_xchg(&(p->y), (p->y) + v->yVel);
		}
	}
	if (p->x + r > 1.0f || p->x - r < -1.0f){
		v->xVel *= -1;
		while (p->x + r > 1.0f || p->x - r < -1.0f){
			//p->x += v->xVel;
			atomic_xchg(&(p->x), (p->x) + v->xVel);
		}
	}
}

//Collide physics logic
void collide(__global Position* p1, __global Velocity* v1, float r1, __global Position* p2, __global Velocity* v2, float r2){
	float u1, u2, m1, m2, newXVel, newYVel, newXVelC, newYVelC;
	u1 = v1->xVel;
	u2 = v2->xVel;
	m1 = r1;
	m2 = r2;

	newXVel = (u1*(m1 - m2) + 2 * m2*u2) / (m1 + m2);
	newXVelC = (u2*(m2 - m1) + 2 * m1*u1) / (m1 + m2);

	u1 = v1->yVel;
	u2 = v2->yVel;
	m1 = r1;
	m2 = r2;

	newYVel = (u1*(m1 - m2) + 2 * m2*u2) / (m1 + m2);
	newYVelC = (u2*(m2 - m1) + 2 * m1*u1) / (m1 + m2);

	v1->xVel = newXVel;
	v1->yVel = newYVel;

	v2->xVel = newXVelC;
	v2->yVel = newYVelC;
}

__kernel void DetectCollisionKernel(__global Position* posGpuBuf, __global Position* ballPositions, __global Velocity* ballVelocities, __global float* radius, __global unsigned char* mappedData, __global unsigned int* collided, int zoneNumber, int numOfBalls){

	int startingIndex = zoneNumber * numOfBalls;
	for (int i = 0; i < numOfBalls; ++i){
		if (!mappedData[startingIndex + i]) continue;
		for (int j = i + 1; j < numOfBalls; ++j){
			if (mappedData[startingIndex + j]){
				float dist = GetDistance(&ballPositions[i], &ballPositions[j]);
				if (dist <= radius[i] + radius[j]){
					int old = atomic_add(collided + i, 1);
					if (!old){
						collide(ballPositions + i, ballVelocities + i, radius[i], ballPositions + j, ballVelocities + j, radius[j]);
						//If the balls are stuck, unstick them
						while (dist <= radius[i] + radius[j]){
							SimplifiedMove(ballPositions + i, ballVelocities + i, radius[i]);
							SimplifiedMove(ballPositions + j, ballVelocities + j, radius[j]);
							dist = GetDistance(&ballPositions[i], &ballPositions[j]);
						}
						posGpuBuf[i].x = ballPositions[i].x;
						posGpuBuf[i].y = ballPositions[i].y;
						posGpuBuf[j].x = ballPositions[j].x;
						posGpuBuf[j].y = ballPositions[j].y;
					}
				}
			}
		}
	}

}


__kernel void MapKernel(__global Position* ballPosition, float radius, __global ZoneBounds* bounds, __global MapData* mapData, __global unsigned char* mappedData, int numOfBalls, int ballNumber){
	float x = ballPosition->x + 1.0f;
	float y = ballPosition->y * -1 + 1.0f;

	//To avoid access violations
	if (x > 2.0f) x = 1.999999f;
	if (y > 2.0f) y = 1.999999f;
	if (x < 0.0f) x = 0.0f;
	if (y < 0.0f) y = 0.0f;

	int col = x / mapData->horizontalSplit;
	int row = y / mapData->verticalSplit;

	int zoneIndex = (row * mapData->cols) + col;

	mappedData[(zoneIndex * numOfBalls) + ballNumber] = 1;
	__global ZoneBounds* cb = bounds + zoneIndex;
	int newZoneIndex;

	//If the circle overlaps 2 zones, we add it to the list of circles to check for both zones
	if (cb->leftBound != -1.0f && ballPosition->x - radius <= cb->leftBound){
		//printf("a\n");
		newZoneIndex = (row * mapData->cols) + col - 1;
		mappedData[(newZoneIndex * numOfBalls) + ballNumber] = 1;
	}
	if (cb->upperBound != 1.0f && ballPosition->y + radius >= cb->upperBound){
		//printf("b\n");
		newZoneIndex = ((row - 1) * mapData->cols) + col;
		mappedData[(newZoneIndex * numOfBalls) + ballNumber] = 1;
	}
	if (cb->rightBound != 1.0f && ballPosition->x + radius >= cb->rightBound){
		//printf("c\n");
		newZoneIndex = (row * mapData->cols) + col + 1;
		mappedData[(newZoneIndex * numOfBalls) + ballNumber] = 1;
	}
	if (cb->lowerBound != -1.0f && ballPosition->y - radius <= cb->lowerBound){
		//printf("d\n");
		newZoneIndex = ((row + 1) * mapData->cols) + col;
		mappedData[(newZoneIndex * numOfBalls) + ballNumber] = 1;
	}
}