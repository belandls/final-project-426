#include "kernel.h.cl"

__constant float NUM_OF_SEGMENTS = 100.0f;

__kernel void UpdateVerticesKernel(__global Position* gBallPosition, __global float2* coord, __global float* radius){

	size_t lid = get_global_id(0);
	
	int nos = NUM_OF_SEGMENTS;
	int index = lid / nos;
	
	float theta = 2.0f * M_PI_F * (lid % nos) / nos;

	float x = radius[index] * cos(theta);
	float y = radius[index] * sin(theta);
	x += gBallPosition[index].x;
	y += gBallPosition[index].y;


	coord[lid] = (float2)(x, y);

}