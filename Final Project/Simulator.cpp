/*
Author : Samuel Beland-Leblanc (7185642)
File : Simulator.cpp

The simulator manages the simulation resources (Circles, pipeline, etc.)

*/

#define C_LOG(s) std::cout<< s << std::endl;
#include "glew.h"
#include "Simulator.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>  /* defines FILENAME_MAX */
#include <direct.h>
#include <ctime>
#define GetCurrentDir _getcwd


const int Simulator::SPEED_DIVIDER = 15;	//For random speed generation
const int Simulator::POSITION_RANGE = 180;  //for random position genration
const int Simulator::RADIUS_RANGE = 2;		//For random radius generation

void eh2(cl_int err){
	if (err){
		std::cout << err << std::endl;
		system("PAUSE");
	}
}

Simulator::Simulator(int numOfBalls, int columns, int rows)
{
	_cl_err = 0;
	_cpu = NULL;
	_gpu = NULL;

	InitializeCL();
	InitializeMainProgram();
	InitializeGPUProgram();

	GenerateTaskQueues();

	_col_map = new CollisionMap(columns, rows,numOfBalls,_cpu_context);	
	_data_parallel_queue = new cl::CommandQueue(*_gpu_context, *_gpu,  CL_QUEUE_PROFILING_ENABLE, &_cl_err);
	
	//Generate the buffer the share between CPU/GPU for positions
	_position_for_mapbuffer = new Circle::Position[numOfBalls];
	_gpu_position_buffer = new cl::Buffer(*_gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(Circle::Position) * numOfBalls,&_cl_err);

	_initial_vbo_data = new GLfloat[100 * 3 * numOfBalls]; //Initial Color data
	_r = new float[numOfBalls]; //Radius array to be used with kernels buffer

	//Generate the circles and the required threads and thread task
	for (int i = 0; i < numOfBalls; ++i){

		float r = ((rand() % RADIUS_RANGE) + 1.0f) / 100.0f;
		float x = ((rand() % POSITION_RANGE) + 10) / 100.0f;
		float y = ((rand() % POSITION_RANGE) + 10) / 100.0f;
		float sx = (rand() % SPEED_DIVIDER) / (float)SPEED_DIVIDER;
		float sy = (rand() % SPEED_DIVIDER) / (float)SPEED_DIVIDER;
		sx /= 100.0f;
		sy /= 100.0f;
		x -= 1;
		y -= 1;
		_r[i] = r;
		Circle* c = new Circle(i, r, x, y, sx, sy, _main_program, _cpu_context, _gpu_program, _gpu_context, numOfBalls, i, _col_map);
		c->SetColor(rand() % 255, COLOR::RED, _initial_vbo_data);
		c->SetColor(rand() % 255, COLOR::GREEN, _initial_vbo_data);
		c->SetColor(rand() % 255, COLOR::BLUE, _initial_vbo_data);

		_circles.push_back(c);
	}

	//Sets up a VAO with 2 buffers(VBO), 1 for the vertices and 1 for the colors
	_vbos = new GLuint[2];
	glGenVertexArrays(1, &_vao);
	glGenBuffers(2, _vbos);

	//Vertices position
	glBindVertexArray(_vao);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, 2 * numOfBalls * 100*sizeof(GLfloat), NULL, GL_STREAM_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);

	//Vertices color
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, _vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, 3 * 100*sizeof(GLfloat) * numOfBalls, _initial_vbo_data, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	_glb = new cl::BufferGL(*_gpu_context, CL_MEM_WRITE_ONLY, _vbos[0]); //GL buffer for the vertex buffer (VBO) binding in OCL
	_rb = new cl::Buffer(*_gpu_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, numOfBalls * sizeof(float), _r); //fixed radius buffer to send the radius on the gpu

	//Set up the vertices kernel
	_vertices_kernel = new cl::Kernel(*_gpu_program, "UpdateVerticesKernel");
	_vertices_kernel->setArg(0, *_gpu_position_buffer);
	_vertices_kernel->setArg(1, *_glb);
	_vertices_kernel->setArg(2, *_rb);

	//Transfer the radius on the GPU's memory
	_data_parallel_queue->enqueueWriteBuffer(*_rb, true, 0, sizeof(float)*numOfBalls, _r);

	//Set up the buffers and arrays for the Collision kenrnel
	_collision_positions = new Circle::Position[numOfBalls];
	_collision_velocities = new Circle::Velocity[numOfBalls];
	_shared_collided_data = new unsigned int[numOfBalls]{0};
	_positions_buffer = new cl::Buffer(*_cpu_context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(Circle::Position) * numOfBalls,_collision_positions);
	_velocities_buffer = new cl::Buffer(*_cpu_context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(Circle::Velocity) * numOfBalls, _collision_velocities);
	_radius_buffer = new cl::Buffer(*_cpu_context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float) * numOfBalls, _r);
	_collided_buffer = new cl::Buffer(*_cpu_context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(unsigned int) * numOfBalls, _shared_collided_data);

	//Generate a kernel per collision zone
	_collision_kernels = new cl::Kernel*[_col_map->GetNumOfZones()];
	for (int i = 0; i < _col_map->GetNumOfZones(); ++i){
		_collision_kernels[i] = new cl::Kernel(*_main_program, "DetectCollisionKernel");
		_collision_kernels[i]->setArg(1, *_positions_buffer);
		_collision_kernels[i]->setArg(2, *_velocities_buffer);
		_collision_kernels[i]->setArg(3, *_radius_buffer);
		_collision_kernels[i]->setArg(4, *_col_map->GetMappingBuffer());
		_collision_kernels[i]->setArg(5, *_collided_buffer);
		_collision_kernels[i]->setArg(6, i);
		_collision_kernels[i]->setArg(7, numOfBalls);
	}
	
}


Simulator::~Simulator()
{
	
	delete _main_program;
	delete _gpu_program;
	delete _cpu_context;
	delete _gpu_context;
	delete _cpu;
	delete _gpu;
	for (int i = 0; i < _num_of_task_queues; ++i){
		delete _task_queue[i];
	}
	delete[] _task_queue;
	delete _data_parallel_queue;
	delete _gpu_position_buffer;
	delete[] _position_for_mapbuffer;
	delete[] _initial_vbo_data;
	delete[] _r;
	glDeleteBuffers(2, _vbos);
	glDeleteVertexArrays(1, &_vao);
	delete[] _vbos;
	delete _vertices_kernel;
	delete _rb;
	delete _glb;
	delete[] _collision_positions;
	delete[] _collision_velocities;
	unsigned int* _shared_collided_data;
	delete _positions_buffer;
	delete _velocities_buffer;
	delete _radius_buffer;
	delete _collided_buffer;
	for (int i = 0; i < _col_map->GetNumOfZones(); ++i){
		delete _collision_kernels[i];
	}
	delete[] _collision_kernels;
	delete _col_map;

	for (vector<Circle*>::iterator it = _circles.begin(); it != _circles.end(); ++it){
		delete *it;
	}

}



// Activates the thread pool for the circle computation threads
void Simulator::UpdatePositions(){


	cl_int err;

	//Maps memory on the host for the positions updates between CPU/GPU
	Circle::Position* tempPtr = (Circle::Position*)_data_parallel_queue->enqueueMapBuffer(*_gpu_position_buffer, true, CL_MAP_WRITE,0, sizeof(Circle::Position) * _circles.size(),NULL,NULL,&err);
	cl::Buffer mappedPosBuf(*_cpu_context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(Circle::Position) * _circles.size(), tempPtr, &err);

	//Enqueue the update and map kernels across the task command queues
	int i = 0;
	for (vector<Circle*>::iterator it = _circles.begin(); it != _circles.end(); ++it, ++i){
		cl::CommandQueue* tq = _task_queue[i % _num_of_task_queues];
		(*it)->UpdateAndMap(tq, mappedPosBuf, i);
	}

	//Wait for them to finish
	for (int j = 0; j < _num_of_task_queues; j++){
		clFinish((*_task_queue[j])());
	}

	//Sets the data for the collision detection
	ResetCollidedData();
	ExtractBallData();
	//Enque the collison kernels across the task command queues
	for (int i = 0; i < _col_map->GetNumOfZones(); ++i){
		int queueIndex = i % _num_of_task_queues;
		_collision_kernels[i]->setArg(0, mappedPosBuf);
		_task_queue[queueIndex]->enqueueTask(*_collision_kernels[i], NULL, NULL);
	}

	//wait until they finish
	for (int j = 0; j < _num_of_task_queues; j++){
		clFinish((*_task_queue[j])());
	}
	//Update any data that got modified
	InsertBallData();

	//Unmap the memory
	_data_parallel_queue->enqueueUnmapMemObject(*_gpu_position_buffer, tempPtr);

	_col_map->ResetMapping();
}

void Simulator::DrawCircles(){
	
	std::vector<cl::Memory> bglList;
	bglList.push_back(*_glb);
	glFinish();
	//Get the VBO and enqueue the vertex update. This is done in order because it's an in-order queue
	_data_parallel_queue->enqueueAcquireGLObjects(&bglList);
	_data_parallel_queue->enqueueNDRangeKernel(*_vertices_kernel, cl::NullRange, cl::NDRange(_circles.size() * Circle::NUM_OF_SEGMENTS), cl::NullRange);
	_data_parallel_queue->enqueueReleaseGLObjects(&bglList);
	clFinish((*_data_parallel_queue)()); //To be sure the VBO has been released correctly
	
	//Draw the balls
	glBindVertexArray(_vao);
	glLineWidth(2.0f);
	for (int i = 0; i < _circles.size(); ++i){
		glDrawArrays(GL_LINE_LOOP, i*Circle::NUM_OF_SEGMENTS, Circle::NUM_OF_SEGMENTS);
	}
	glBindVertexArray(0);
}

//draw the collision zone to a visual representation
void Simulator::DrawZones(){

	for (int i = 0; i < _col_map->GetNumOfZones(); i++){
		const CollisionMap::ZoneBounds* b = _col_map->GetBounds() + i;

		glColor3f(0.0f, 0.0f, 0.0f);
		glBegin(GL_LINES);

		glVertex2f(b->leftBound, b->upperBound);
		glVertex2f(b->rightBound, b->upperBound);

		glVertex2f(b->rightBound, b->upperBound);
		glVertex2f(b->rightBound, b->lowerBound);

		glVertex2f(b->rightBound, b->lowerBound);
		glVertex2f(b->leftBound, b->lowerBound);

		glVertex2f(b->leftBound, b->lowerBound);
		glVertex2f(b->leftBound, b->upperBound);

		glEnd();
	}
}

Circle* Simulator::GetCircle(int index){
	if (index >= 0 && index < _circles.size()){
		return _circles[index];
	}
	else{
		return NULL;
	}
}

//Builds the program with the CPU kernels
void Simulator::InitializeMainProgram(){

	char* readBuffer;
	int fileSize;
	ifstream kernelSource("kernels.cl",ios::in);
	if (kernelSource.is_open()){
		string code((std::istreambuf_iterator<char>(kernelSource)), std::istreambuf_iterator<char>());

		char cCurPath[FILENAME_MAX];
		GetCurrentDir(cCurPath, sizeof(cCurPath));
		std::cout << cCurPath << std::endl;

		_main_program = new cl::Program(*_cpu_context, code, false, &_cl_err);

		std::string options("-I \"");
		options.append(cCurPath);
		options.append("\"");

		_cl_err = _main_program->build(options.c_str(), NULL, NULL);
		if (_cl_err){
			std::string s;
			_main_program->getBuildInfo(*_cpu,CL_PROGRAM_BUILD_LOG, &s);
			std::cout << s << std::endl;
		}

	}
	
}

//Builds the program with the the GPU kernels
void Simulator::InitializeGPUProgram(){

	char* readBuffer;
	int fileSize;
	ifstream kernelSource("gpu_kernel.cl", ios::in);
	if (kernelSource.is_open()){
		string code((std::istreambuf_iterator<char>(kernelSource)), std::istreambuf_iterator<char>());

		char cCurPath[FILENAME_MAX];
		GetCurrentDir(cCurPath, sizeof(cCurPath));
		std::cout << cCurPath << std::endl;

		_gpu_program = new cl::Program(*_gpu_context, code, false, &_cl_err);

		std::string options("-I \"");
		options.append(cCurPath);
		options.append("\"");

		_cl_err = _gpu_program->build(options.c_str(), NULL, NULL);
		if (_cl_err){
			std::string s;
			_gpu_program->getBuildInfo(*_gpu, CL_PROGRAM_BUILD_LOG, &s);
			std::cout << s << std::endl;
		}

	}

}

//Initialize opencl devices
bool Simulator::InitializeCL(){

	C_LOG("Initializing OPEN_CL...");

	vector<cl::Platform> platforms;

	_cl_err = cl::Platform::get(&platforms);
	ERR_CHK();

	if (platforms.size() > 0){

		vector<cl::Device> cpus;
		vector<cl::Device> gpus;

		for (auto i = platforms.begin(); i != platforms.end(); ++i){

			

			vector<cl::Device> devBuf;
			_cl_err = (*i).getDevices(CL_DEVICE_TYPE_CPU, &devBuf);
			if (_cl_err){
				if (_cl_err == -1)
					_cl_err = 0;
				else
					ERR_CHK()
			}
			else {
				cpus.insert(cpus.end(), devBuf.begin(), devBuf.end());
				//_cpu = cpus[0];
			}

			_cl_err = (*i).getDevices(CL_DEVICE_TYPE_GPU, &devBuf);
			if (_cl_err){
				if (_cl_err == -1)
					_cl_err = 0;
				else
					ERR_CHK()
			}
			else {
				gpus.insert(gpus.end(), devBuf.begin(), devBuf.end());
				//_gpu = gpus[0];
			}
			
		}


		//If there are multiple device of the same type, choose the fastest one for simplicity
		int freqMax = 0;
		cl::Device* selected;
		for (auto i = cpus.begin(); i != cpus.end(); ++i){
			int freqTemp = 0;
			_cl_err = (*i).getInfo(CL_DEVICE_MAX_CLOCK_FREQUENCY, &freqTemp);
			if (freqTemp > freqMax){
				selected = &(*i);
			}
			ERR_CHK();
		}
		_cpu = new cl::Device(*selected);
		selected = NULL;
		freqMax = 0;

		for (auto i = gpus.begin(); i != gpus.end(); ++i){
			int freqTemp = 0;
			_cl_err = (*i).getInfo(CL_DEVICE_MAX_CLOCK_FREQUENCY, &freqTemp);
			if (freqTemp > freqMax){
				selected = &(*i);
			}
			ERR_CHK();
		}
		_gpu = new cl::Device(*selected);
	}


	//Create the contexts for both devices
	_cpu_context = new cl::Context(*_cpu,NULL,NULL,NULL,&_cl_err);
	ERR_CHK();
	cl_context_properties properties[] = {
		CL_GL_CONTEXT_KHR,
		(cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR,
		(cl_context_properties)wglGetCurrentDC(),
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)_gpu->getInfo<CL_DEVICE_PLATFORM>(), 0 };
	_gpu_context = new cl::Context(*_gpu,properties, NULL, NULL, &_cl_err);
	ERR_CHK();

	return _cl_err;
}

//Generate the task queues depending on the number of compute units on the CPU
void Simulator::GenerateTaskQueues(){
	int numOfProcs = _cpu->getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
	if (numOfProcs >= 3){
		_num_of_task_queues = numOfProcs - 2 ;
	}
	else {
		_num_of_task_queues = 1;
	}
	_task_queue = new cl::CommandQueue*[_num_of_task_queues];
	for (int i = 0; i < _num_of_task_queues; ++i){
		_task_queue[i] = new cl::CommandQueue(*_cpu_context, *_cpu);
	}
}


void Simulator::ExtractBallData(){
	for (int i = 0; i < _circles.size(); ++i){
		_collision_positions[i] = _circles[i]->GetPosition();
		_collision_velocities[i] = _circles[i]->GetVelocity();
		
	}
}

void Simulator::InsertBallData(){
	for (int i = 0; i < _circles.size(); ++i){
		_circles[i]->SetPosition(_collision_positions[i]);
		_circles[i]->SetVelocity(_collision_velocities[i]);
	}
}

void Simulator::ResetCollidedData(){
	for (int i = 0; i < _circles.size(); ++i){
		_shared_collided_data[i] = 0;
	}
}