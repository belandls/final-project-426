//Shared structs

typedef struct {
	float x;
	float y;
} Position;

typedef struct {
	float xVel;
	float yVel;
} Velocity;

typedef struct {
	float leftBound;
	float upperBound;
	float rightBound;
	float lowerBound;
}ZoneBounds;

typedef struct {
	int cols;
	int rows;
	float horizontalSplit;
	float verticalSplit;
}MapData;