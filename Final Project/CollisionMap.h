/*
Author : Samuel Beland-Leblanc (7185642)
File : CollisionMap.h

Contains all the collision zones data

*/


#pragma once
#include "CL\cl.hpp"
//#include "CollisionZone.h"
#include "Circle.h"

class CollisionMap
{
public:
	struct ZoneBounds{
		float leftBound;
		float upperBound;
		float rightBound;
		float lowerBound;
	};
	struct MapData{
		int cols;
		int rows;
		float horizontalSplit;
		float verticalSplit;
	};

private:

	
	int _cols; //num of rows
	int _rows; // num of columns
	float _hori_split; //width for 1 zone
	float _verti_split; //height for 1 zone

	int _num_of_balls;
	MapData _map_data;
	unsigned char* _mapping;

	const cl::Buffer* _bounds_ro_buffer;
	const cl::Buffer* _map_data_ro_buffer;
	const cl::Buffer* _mapping_buffer;
	ZoneBounds* _bounds;
	int _num_of_zones;


	void GenerateMap(int cols, int rows);


public:
	

	CollisionMap(int cols, int rows, int numOfBalls, cl::Context* cpuContext);
	~CollisionMap();

	//CollisionZone** _zones;

	//void MapCircle(Circle* const);
	//void DispatchCollisionChecks();;

	const cl::Buffer* GetBoundsBuffer() const { return _bounds_ro_buffer; };
	const cl::Buffer* GetMapDataBuffer() const { return _map_data_ro_buffer; };
	const cl::Buffer* GetMappingBuffer() const { return _mapping_buffer; };

	int GetBoundsSize() const;
	int GetMapDataSize() const;
	int GetMappingSize() const;

	void ResetMapping();
	void OutputMapping() const;
	int GetNumOfZones() const { return _num_of_zones; };
	ZoneBounds const * const GetBounds() const { return _bounds; };

};

