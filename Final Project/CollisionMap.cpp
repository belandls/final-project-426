/*
Author : Samuel Beland-Leblanc (7185642)
File : CollisionMap.cpp

Contains all the collision zones and maps the circle to the appropriate zones

*/

#include "CollisionMap.h"
#include <iostream>


CollisionMap::CollisionMap(int cols, int rows, int numOfBalls, cl::Context* cpuContext) : _cols(cols), _rows(rows)
{
	_num_of_balls = numOfBalls;
	_map_data.cols = cols;
	_map_data.rows = rows;

	_num_of_zones = rows*cols;

	_bounds = new ZoneBounds[_num_of_zones];
	_mapping = new unsigned char[_num_of_zones * numOfBalls]{};

	GenerateMap(cols, rows);

	//Generate Buffers shared across kernels
	_bounds_ro_buffer = new cl::Buffer(*cpuContext, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, GetBoundsSize(), _bounds);
	_map_data_ro_buffer = new cl::Buffer(*cpuContext, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, GetMapDataSize(), &_map_data);
	_mapping_buffer = new cl::Buffer(*cpuContext, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, GetMappingSize(), _mapping);
}

int CollisionMap::GetBoundsSize() const{
	return sizeof(CollisionMap::ZoneBounds) * _num_of_zones;
}

int CollisionMap::GetMapDataSize() const{
	return sizeof(CollisionMap::MapData);
}

int CollisionMap::GetMappingSize() const{
	return sizeof(unsigned char) * _num_of_zones * _num_of_balls;
}

CollisionMap::~CollisionMap()
{
	delete _bounds_ro_buffer;
	delete _map_data_ro_buffer;
	delete _mapping_buffer;
	delete[] _mapping;
	delete[] _bounds;
}


//Generates the collision zones
void CollisionMap::GenerateMap(int cols, int rows) {

	_map_data.horizontalSplit = (float)2 / cols;
	_map_data.verticalSplit = (float)2 / rows;
	
	_hori_split = (float)2 / cols; // 2 = -1 to 1
	_verti_split = (float)2 / rows; // 2 = -1 to 1

	for (int row = 0; row < rows; ++row){
		for (int col = 0; col < cols; ++col){
			int index = (row * cols) + col;

			_bounds[index].leftBound = (float)col*_hori_split - 1;
			_bounds[index].upperBound = ((float)row*_verti_split - 1) * -1;
			_bounds[index].rightBound = (float)(col + 1)*_hori_split - 1;
			_bounds[index].lowerBound = ((float)(row + 1)*_verti_split - 1) * -1;
		}
	}

}

void CollisionMap::ResetMapping(){
	for (int i = 0; i < _num_of_zones * _num_of_balls; ++i){
		_mapping[i] = 0;
	}
}

//For debugging purposes. Will output the maping
void CollisionMap::OutputMapping() const{
	for (int i = 0; i < _num_of_zones * _num_of_balls; ++i){
		std::cout << (int)_mapping[i] << " ";
	}
	std::cout << std::endl;
}

