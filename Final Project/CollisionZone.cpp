/*
Author : Samuel Beland-Leblanc (7185642)
File : CollisionZone.cpp

The colision zone manages the colision detection for a sub part of the screen.

*/

#include "CollisionZone.h"
#include <iostream>


CollisionZone::CollisionZone(float leftB, float upperB, float rightB, float lowerB) : _left_bound(leftB), _upper_bound(upperB), _right_bound(rightB), _lower_bound(lowerB){
	this->_circles_to_check = new vector<Circle*>();
}

CollisionZone::~CollisionZone()
{
	//std::cout << "size : " << _circles_to_check->size() << std::endl;
	delete this->_circles_to_check;
}

void CollisionZone::AddCircleToCheck(Circle* c){
	this->_circles_to_check->push_back(c);
}

//Detect collisions between the circles in the current zone
void CollisionZone::CheckForCollisions(){

	for (vector<Circle*>::iterator it1 = _circles_to_check->begin(); it1 != _circles_to_check->end(); ++it1){
		for (vector<Circle*>::iterator it2 = it1 + 1; it2 != _circles_to_check->end(); ++it2){
			if (*it1 == *it2){
				continue;
			}
			else{
				(*it1)->CheckForCollision(*it2);
			}
		}
	}
	_circles_to_check->clear();

}

//Check to be sure 2 adjacent zones don't have the same circle list, because the collision will be handled 
//twice, thus cancelling the collision.
bool CollisionZone::CheckForDuplicate(CollisionZone* const czSource){
	for (vector<Circle*>::iterator it1 = czSource->_circles_to_check->begin(); it1 != czSource->_circles_to_check->end(); ++it1){
		for (vector<Circle*>::iterator it2 = _circles_to_check->begin(); it2 != _circles_to_check->end(); ++it2){
			if (*it1 == *it2)
				return true;
		}
	}
	return false;
}