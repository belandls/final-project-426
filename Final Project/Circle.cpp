/*
Author : Samuel Beland-Leblanc (7185642)
File : Circle.cpp

Moving and Drawing logic for the bouncing circles

*/
#define _USE_MATH_DEFINES
#include <glew.h>
#include "Circle.h"
#include "CollisionMap.h"
#include <iostream>
#include <math.h>

const int Circle::NUM_OF_SEGMENTS = 100; //Used for the circle drawing algorithme
const float Circle::ACCELERATION = 0.001; //Gravity acceleration constant

Circle::Circle(int id, float r, float x, float y, float xVel, float yVel, cl::Program* mainProgram, cl::Context* cpuContext, cl::Program* gpuProgram, cl::Context* gpuContext, int numOfBalls, int ballNumber, CollisionMap* cm) : _radius(r), _x(x), _y(y), _x_vel(xVel), _y_vel(yVel), _id(id)
{
	_position.x = _x;
	_position.y = _y;
	_velocity.xVel = _x_vel;
	_velocity.yVel = _y_vel;

	//Sets up buffer for update and map kernels
	_position_buffer = new cl::Buffer(*cpuContext, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(Circle::Position), &_position);
	_velocity_buffer = new cl::Buffer(*cpuContext, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(Circle::Velocity), &_velocity);


	cl_int errl;
	//Set up position update kernel (1 per ball)
	_position_kernel = new cl::Kernel(*mainProgram, "UpdateKernel",&errl);
	_position_kernel->setArg(0, *_position_buffer);
	_position_kernel->setArg(1, *_velocity_buffer);
	_position_kernel->setArg(2, ACCELERATION);
	_position_kernel->setArg(3, _radius);

	//Set up position mapping kernel (1 per ball)
	_map_kernel = new cl::Kernel(*mainProgram, "MapKernel", &errl);
	_map_kernel->setArg(0, *_position_buffer);
	_map_kernel->setArg(1, _radius);
	_map_kernel->setArg(2, *(cm->GetBoundsBuffer()));
	_map_kernel->setArg(3, *(cm->GetMapDataBuffer()));
	_map_kernel->setArg(4, *(cm->GetMappingBuffer()));
	_map_kernel->setArg(5, numOfBalls); 
	_map_kernel->setArg(6, ballNumber);

}


Circle::~Circle()
{
	delete _position_buffer;
	delete  _velocity_buffer;
	delete  _radius_ro_buffer;

	delete _position_kernel;
	delete _map_kernel;
}

//Sets color in the initial VBO array
void Circle::SetColor(int val, COLOR c, GLfloat* cData){
	int colTemp;
	if (val > 255)
		colTemp = 255;
	else if (val < 0)
		colTemp = 0;
	else
		colTemp = val;

	int init = _id * NUM_OF_SEGMENTS;
	int end = (_id+1) * NUM_OF_SEGMENTS;

	switch (c)
	{
	case RED:
		for (int i = init; i < end; ++i){
			cData[i*3] = colTemp /255.0f;
		}
		break;
	case GREEN:
		for (int i = init; i < end; ++i){
			cData[i*3 + 1] = colTemp / 255.0f;
		}
		break;
	case BLUE:
		for (int i = init; i < end; ++i){
			cData[i*3 + 2] = colTemp / 255.0f;
		}
		break;
	default:
		break;
	}

}

void eh(cl_int err){
	if (err){
		std::cout << err << std::endl;
		system("PAUSE");
	}
}

//Enqueues the update and map buffers on the givin queue
void Circle::UpdateAndMap(cl::CommandQueue* cq, cl::Buffer gpuPosBuf, int n){
	
	cl_int err;
	_position_kernel->setArg(4, gpuPosBuf); // setting gpu position buffer
	_position_kernel->setArg(5, _id); //setting balll number

	err = cq->enqueueTask(*_position_kernel, NULL, NULL);
	eh(err);

	err = cq->enqueueTask(*_map_kernel, NULL, NULL);
	eh(err);
}

