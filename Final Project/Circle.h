/*
Author : Samuel Beland-Leblanc (7185642)
File : Circle.h

Moving logic for the bouncing circles

*/
class CollisionMap;
#pragma once
#include <mutex>
//#include "CollisionMap.h"
#include <CL\cl.hpp>


enum COLOR{
	RED,
	GREEN,
	BLUE
};

class Circle
{
public:
	struct coordinate{
		float x;
		float y;
	};
	struct Position{
		float x;
		float y;
	};
	struct Velocity{
		float xVel;
		float yVel;
	};
private:

	
	static const float ACCELERATION;

	
	Velocity _velocity;
	
	int _id;
	float _x;
	float _y;
	float _x_vel;
	float _y_vel;
	int _color[3]; //color vector (RGB)
	std::mutex _colliding_mutex; //mutex in case of multiple collisions

	float GetDistanceFromOtherCircle(Circle* const); 

	cl::Buffer* _position_buffer;
	cl::Buffer* _velocity_buffer;
	cl::Buffer* _radius_ro_buffer;
	
	cl::Kernel* _position_kernel;
	cl::Kernel* _map_kernel;

	Position _position;
	float _radius;

public:
	static const int NUM_OF_SEGMENTS;
	
	
	Circle(int id, float r, float x, float y, float xVel, float yVel, cl::Program*, cl::Context*, cl::Program* gpuProgram, cl::Context* gpuContext, int numOfBalls, int ballNumber, CollisionMap* cm);
	~Circle();
	void UpdateAndMap(cl::CommandQueue*, cl::Buffer posTest, int n);
	void SetColor(int, COLOR, GLfloat* cData);

	float getX() { return _position.x; };
	float getY() { return _position.y; };
	float getRadius() { return _radius; };
	Position GetPosition() const { return _position; };
	void SetPosition(Position& p) { _position = p; };
	Velocity GetVelocity() const { return _velocity; };
	void SetVelocity(Velocity& p) { _velocity = p; };

};

