/*
Author : Samuel Beland-Leblanc (7185642)
File : Simulator.h

The simulator manages the simulation resources (Circles, threads, etc.)

*/

#pragma once

#define ERR_CHK() if(_cl_err) return false;
#include "CL\cl.hpp"
#include <vector>
#include "Circle.h"
#include "CollisionMap.h"

using namespace std;

class Simulator
{
private:

	static const int SPEED_DIVIDER;  //For random speed generation
	static const int POSITION_RANGE; //for random position genration
	static const int RADIUS_RANGE;	 //For random radius generation

	vector<Circle*> _circles; //circles on screen
	CollisionMap* _col_map; //the current collision map

	cl::Program* _main_program;
	cl::Program* _gpu_program;
	cl::Context* _cpu_context; //You cannot combine 2 platforms in the same context. CPU/GPU are most likely on 2 dif. platform (Nvidia OCL and Intel OCL)
	cl::Context* _gpu_context; 
	cl::Device* _cpu;
	cl::Device* _gpu;
	cl::CommandQueue** _task_queue;
	int _num_of_task_queues;
	cl::CommandQueue* _data_parallel_queue;
	cl_int _cl_err;

	cl::Buffer* _gpu_position_buffer;
	Circle::Position* _position_for_mapbuffer;

	GLfloat* _initial_vbo_data;
	float* _r;
	GLuint* _vbos;
	GLuint _vao;
	cl::Kernel* _vertices_kernel;
	cl::Buffer* _rb;
	cl::BufferGL* _glb;

	Circle::Position* _collision_positions;
	Circle::Velocity* _collision_velocities;
	unsigned int* _shared_collided_data;
	cl::Buffer* _positions_buffer;
	cl::Buffer* _velocities_buffer;
	cl::Buffer* _radius_buffer;
	cl::Buffer* _collided_buffer;
	cl::Kernel** _collision_kernels;


	bool InitializeCL();
	void InitializeMainProgram();
	void InitializeGPUProgram();
	void GenerateTaskQueues();

	void ExtractBallData();
	void InsertBallData();
	void ResetCollidedData(); 

public:
	Simulator(int, int = 3, int = 2);
	~Simulator();

	

	void UpdatePositions();
	void DrawCircles();
	void DrawZones();
	Circle* GetCircle(int);
	CollisionMap* GetColMap(){ return _col_map; };

	

};

