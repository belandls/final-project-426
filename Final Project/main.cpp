/*
Author : Samuel Beland-Leblanc (7185642)
File : main.cpp

Entry point and main loop

*/

#define GLEW_STATIC
#include "glew.h"
#include <windows.h>
#include "freeglut.h"
#include <chrono>
#include <math.h>
#include "Circle.h"
#include "CollisionMap.h"
#include <vector>
#include "Simulator.h"
#include <time.h>
#include <iostream>
#include <string>

using namespace std;

const std::string vertexShaderSource =
"#version 330 \n"
"in layout(location = 0) vec2 in_Position;"
"in layout(location = 1) vec3 in_Color;"
"out vec3 out_Color; "
"void main(void){"
"gl_Position = vec4(in_Position, 0.0, 1.0);"
"out_Color = in_Color;"
"}";

const std::string fragmentShaderSource =
"#version 330 \n"
"in vec3 out_Color;"
"out vec3 oc;"
"void main(void){"
"oc = vec3(out_Color);"
"}";

Simulator* sim;

//Compiles/Builds both openGL shaders and binds them
void initShaders(){
	GLuint vs, fs, prog;

	vs = glCreateShader(GL_VERTEX_SHADER);
	fs = glCreateShader(GL_FRAGMENT_SHADER);

	const char *vsource_str = vertexShaderSource.c_str();
	glShaderSource(vs, 1, &vsource_str, NULL);

	const char *fsource_str = fragmentShaderSource.c_str();
	glShaderSource(fs, 1, &fsource_str, NULL);

	GLint success;
	GLsizei log_size;
	GLchar *log;
	glCompileShader(vs);
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderiv(vs, GL_INFO_LOG_LENGTH,
			&log_size);
		log = (char*)malloc(log_size + 1);
		log[log_size] = '\0';
		glGetShaderInfoLog(vs, log_size + 1,
			NULL, log);
		printf("%s\n", log);
		free(log);
		exit(1);
	}
	glCompileShader(fs);
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH,
			&log_size);
		log = (char*)malloc(log_size + 1);
		log[log_size] = '\0';
		glGetShaderInfoLog(fs, log_size + 1,
			NULL, log);
		printf("%s\n", log);
		free(log);
		exit(1);
	}

	prog = glCreateProgram();

	glAttachShader(prog, vs);
	glAttachShader(prog, fs);

	glLinkProgram(prog);
	glUseProgram(prog);

}

void update(){
	sim->UpdatePositions();
}


void display()
{
	
	glClear(GL_COLOR_BUFFER_BIT);

	sim->DrawCircles();
	sim->DrawZones();
	glutSwapBuffers();
}

void idle(){
	update();
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Assignment 1");
	glutDisplayFunc(display);
	glutIdleFunc(idle);

	glewExperimental = true;
	glewInit();

	srand(time(NULL));
	initShaders();

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f); //Sets the backgorund white

	//create new simulation with random num of balls from 3 to 10 with 24 collision zones (6x4 collision map)
	sim = new Simulator(70, 6, 4);
	
	glutMainLoop();
	return 0;
}